
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graduationproject/pages/model/travelModel.dart';

Widget detailCard(TravelModel travelModel) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Card(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.car_repair,
                size: 40,
              ),
              SizedBox(
                width: 30,
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "From : " + travelModel.fromCity,
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        "To      : " + travelModel.toCity,
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.alarm,
                  size: 30,
                  color: Colors.red,
                ),
                SizedBox(
                  width: 30,
                ),
                Text(
                  "Time  : " + travelModel.time,
                  style: TextStyle(fontSize: 20),
                )
              ],
            ),
          )
        ],
      ),
    ),
  );
}
