import 'package:flutter/material.dart';
import 'package:graduationproject/pages/controller/addTravelPage.dart';
import 'package:graduationproject/utiltes/constant.dart';

import '../view/cardDesigin.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('Home'),
      ),
      body: Stack(
        alignment: AlignmentDirectional.bottomEnd,
        children: [
          travelsData.length > 0 ? Container(
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemCount: travelsData.length,
              itemBuilder: (context , index){
                return detailCard(travelsData[index]);
              },
            ),
          ) : Center(
            child: Text('No Travels Data'),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FloatingActionButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddTravelPage()));
                print(travelsData.length);
              },
              child: Icon(Icons.add),
            ),
          ),
        ],
      ),
    );
  }

}
