
import 'package:day_night_time_picker/lib/daynight_timepicker.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:graduationproject/pages/controller/homePage.dart';
import 'package:graduationproject/pages/model/travelModel.dart';
import 'package:graduationproject/utiltes/alert.dart';
import 'package:graduationproject/utiltes/constant.dart';

class AddTravelPage extends StatefulWidget {
  @override
  _AddTravelPageState createState() => _AddTravelPageState();
}

class _AddTravelPageState extends State<AddTravelPage> {
  final _formKey = GlobalKey<FormState>();

  var selectedFromCity = "Selected From City";
  var selectedToCity = "Selected To City";
  var selecteDriver = "Selected Driver";
  TimeOfDay _time = TimeOfDay.now();

  bool isHidden = true;
  bool cityFromIsSelected = false;
  bool cityToIsSelected = false;
  bool timeIsSelected = false;
  bool driverIsSelected = false;

  String fullTimeConvert = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add your Travel Time"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            onChanged: () {
              setState(() {
                cityToIsSelected &&
                    cityFromIsSelected &&
                    driverIsSelected &&
                    timeIsSelected
                    ? isHidden = false
                    : isHidden = true;
              });
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownSearch<String>(
                      mode: Mode.MENU,
                      showSelectedItem: true,
                      items: cities,
                      label: "From",
                      hint: "City in menu mode",
                      popupItemDisabled: (String s) => s.startsWith('I'),
                      onChanged: (value) {
                        setState(() {
                          if (value == selectedToCity) {
                            print("No one");
                            cityFromIsSelected = false;

                            return showAlertDialog(context,
                                "From City Should be Diffrent To City");
                          } else {
                            selectedFromCity = value;
                            cityFromIsSelected = true;
                          }
                        });
                      },
                      selectedItem: selectedFromCity),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownSearch<String>(
                      mode: Mode.MENU,
                      showSelectedItem: true,
                      items: cities,
                      label: "To",
                      hint: "City in menu mode",
                      popupItemDisabled: (String s) => s.startsWith('I'),
                      onChanged: (value) {
                        setState(() {
                          if (value == selectedFromCity) {
                            print("No data");
                            selectedToCity = "Selected To City";
                            cityToIsSelected = false;

                            return showAlertDialog(context,
                                "From City Should be Diffrent To City");
                          } else {
                            selectedToCity = value;
                            cityToIsSelected = true;
                          }
                        });
                      },
                      selectedItem: selectedToCity),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                      color: Colors.blueGrey,
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.alarm_add,
                            color: Colors.white,
                            size: 30,
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).push(
                                showPicker(
                                  context: context,
                                  value: _time,
                                  onChange: (value) {
                                    // fullTimeConvert = _time.hour.toString() +":"+ _time.minute.toString() ;
                                    fullTimeConvert = value.hour.toString() +
                                        ":" +
                                        value.minute.toString();
                                    print(value);

                                    timeIsSelected = true;
                                  },
                                ),
                              );
                            },
                            child: Text(
                              "Select Travel Time",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownSearch<String>(
                      mode: Mode.MENU,
                      showSelectedItem: true,
                      items: drivers,
                      label: "Choice Favourite Diver",
                      hint: "Divers in menu mode",
                      popupItemDisabled: (String s) => s.startsWith('I'),
                      onChanged: (value) {
                        setState(() {
                          selecteDriver = value;
                          driverIsSelected = true;
                        });
                      },
                      selectedItem: selecteDriver),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                    width: double.infinity,
                    height: 45,
                    child: FlatButton(
                      color: Colors.red,
                      disabledColor: Colors.grey,
                      onPressed: isHidden ? null : addTravelData,
                      child: Text(
                        "Add Travel",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  addTravelData() {
    travelsData.add(
      TravelModel(
          fromCity: selectedFromCity,
          time: fullTimeConvert,
          toCity: selectedToCity,
          driverName: selecteDriver),
    );
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => HomePage()));
    print(travelsData.length);
  }
}
